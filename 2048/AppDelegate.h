//
//  AppDelegate.h
//  2048
//
//  Created by Aaron Jiji on 1/28/16.
//  Copyright © 2016 Aaron Jiji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

